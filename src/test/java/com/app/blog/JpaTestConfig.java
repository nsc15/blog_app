package com.app.blog;

import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

@Configuration
public class JpaTestConfig {

    public DataSource datasource() {
        var datasource = new DriverManagerDataSource();
        datasource.setDriverClassName("org.h2.driver");
        datasource.setUrl("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1");
        return datasource;
    }
}
