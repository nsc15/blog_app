package com.app.blog.users;

import com.app.blog.users.dto.CreateUserRequestDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("test")
public class UserServiceTest {

    @Autowired
    private UserServiceImpl userService;

    @Test
    void create_users() {
        var user = userService.createUser(new CreateUserRequestDto("Sai",
                "fhdjash",
                "sai@gmail.com"
        ));
        Assertions.assertNotNull(user);
        Assertions.assertEquals("sai", user.getUsername());
    }
}
