package com.app.blog.comments;

import com.app.blog.articles.ArticleRepo;
import com.app.blog.users.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CommentServiceImpl implements CommentService {
    @Autowired
    private CommentRepo commentRepo;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private ArticleRepo articleRepo;
}
