package com.app.blog.articles;

import com.app.blog.articles.dto.CreateArticleRequestDto;
import com.app.blog.articles.dto.UpdateArticleRequestDto;
import com.app.blog.users.UserRepo;
import com.app.blog.users.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ArticleServiceImpl implements ArticleService {
    @Autowired
    private ArticleRepo articleRepo;

    @Autowired
    private UserRepo userRepo;

    @Override
    public ArticleEntity createArticle(CreateArticleRequestDto dto, Long authorId) {
        var user = this.userRepo
                .findById(authorId)
                .orElseThrow(() -> new UserServiceImpl.UserNotFoundException(authorId));

        return this.articleRepo.save(
                ArticleEntity.builder()
                        .title(dto.getTitle())
                        .body(dto.getBody())
                        .slug(dto.getTitle().toLowerCase().replaceAll("\\s+", "-"))
                        .author(user)
                        .build()
        );
    }

    @Override
    public Iterable<ArticleEntity> getAllArticles() {
        return this.articleRepo.findAll();
    }

    @Override
    public Optional<ArticleEntity> getArticleBySlug(String slug) {
        var articles = this.articleRepo.findBySlug(slug);

        if (articles.isEmpty()) throw new ArticleNotFoundException(slug);
        return articles;
    }

    @Override
    public Optional<ArticleEntity> updateArticle(Long articleId, UpdateArticleRequestDto dto) {
        var article = this.articleRepo.findById(articleId).orElseThrow(() -> new ArticleNotFoundException(articleId));

        if (dto.getTitle() != null) {
            article.setTitle(dto.getTitle());
            article.setSlug(dto.getTitle().toLowerCase().replaceAll("\\s+", "-"));
        }
        if (dto.getBody() != null) article.setBody(dto.getBody());
        if (dto.getSubtitle() != null) article.setSubtitle(dto.getSubtitle());

        return Optional.of(this.articleRepo.save(article));
    }

    static class ArticleNotFoundException extends IllegalArgumentException {
        public ArticleNotFoundException(String slug) {
            super(String.format("Article %s not found", slug));
        }

        public ArticleNotFoundException(Long articleId) {
            super(String.format("Article id %s not found", articleId));
        }
    }
}
