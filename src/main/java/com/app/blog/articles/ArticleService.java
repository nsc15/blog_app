package com.app.blog.articles;

import com.app.blog.articles.dto.CreateArticleRequestDto;
import com.app.blog.articles.dto.UpdateArticleRequestDto;

import java.util.Optional;

public interface ArticleService {
    public ArticleEntity createArticle(CreateArticleRequestDto dto, Long authorId);

    public Iterable<ArticleEntity> getAllArticles();

    public Optional<ArticleEntity> getArticleBySlug(String slug);

    public Optional<ArticleEntity> updateArticle(Long articleId, UpdateArticleRequestDto dto);
}
