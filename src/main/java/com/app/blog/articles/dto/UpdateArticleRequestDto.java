package com.app.blog.articles.dto;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;
import org.springframework.lang.Nullable;

@Data
@Setter(AccessLevel.NONE)
public class UpdateArticleRequestDto {
    @Nullable
    private String title;

    @Nullable
    private String body;

    @Nullable
    public String subtitle;
}
