package com.app.blog.users;

import com.app.blog.users.dto.CreateUserRequestDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public UserEntity createUser(CreateUserRequestDto req) {
//        var newUser = UserEntity.builder()
//                .email(req.getEmail())
////                .password(req.getPassword()) //TODO: encrypt the password
//                .username(req.getUsername()).build();
        //TODO: encrypt the password and save it
        var newUser = modelMapper.map(req, UserEntity.class);
        return userRepo.save(newUser);
    }

    @Override
    public UserEntity getUser(String username) {
        return this.userRepo.findByUsername(username).orElseThrow(() -> new UserNotFoundException(username));
    }

    public UserEntity getUser(Long id) {
        return this.userRepo.findById(id).orElseThrow(() -> new UserNotFoundException(id));
    }

    public UserEntity getUserByEmail(String email) {
        return this.userRepo.findByEmail(email);
    }

    @Override
    public UserEntity loginUser(String username, String password) {
        // TODO: check the password
        return this.getUser(username);
    }


    public static class UserNotFoundException extends IllegalArgumentException {
        public UserNotFoundException(String username) {
            super(String.format("User %s not found", username));
        }

        public UserNotFoundException(Long userId) {
            super(String.format("User ID %s not found", userId));
        }
    }

}
