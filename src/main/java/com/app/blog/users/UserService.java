package com.app.blog.users;

import com.app.blog.users.dto.CreateUserRequestDto;

public interface UserService {
    public UserEntity createUser(CreateUserRequestDto req);

    public UserEntity getUser(String username);

    public UserEntity getUser(Long userId);

    public UserEntity getUserByEmail(String email);

    public UserEntity loginUser(String username, String password);
}
