package com.app.blog.users;

import com.app.blog.common.dto.ErrorResponseDto;
import com.app.blog.users.dto.CreateUserRequestDto;
import com.app.blog.users.dto.CreateUserResponseDto;
import com.app.blog.users.dto.LoginUserRequestDto;
import com.app.blog.users.dto.UserResponseDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping("/listAll")
    String getUsers() {
        return "users";
    }

    @PostMapping("/signup")
    ResponseEntity<CreateUserResponseDto> signupUser(@RequestBody CreateUserRequestDto requestDto) {
        UserEntity savedUser = this.userService.createUser(requestDto);
        URI savedUserUrl = URI.create("/users" + savedUser.getId());
        return ResponseEntity
                .created(savedUserUrl)
                .body(modelMapper.map(savedUser, CreateUserResponseDto.class));
    }

    @PostMapping("/login")
    ResponseEntity<UserResponseDto> loginUser(@RequestBody LoginUserRequestDto requestDto) {
        UserEntity loggedInUser = this.userService.loginUser(requestDto.getUsername(), requestDto.getPassword());
        return ResponseEntity.ok(modelMapper.map(loggedInUser, UserResponseDto.class));
    }

    @ExceptionHandler({
            UserServiceImpl.UserNotFoundException.class
    })
    ResponseEntity<ErrorResponseDto> handleUserNotFoundException(Exception e) {
        String message;
        HttpStatus httpStatus;
        if (e instanceof UserServiceImpl.UserNotFoundException) {
            message = e.getMessage();
            httpStatus = HttpStatus.NOT_FOUND;
        } else {
            message = "Something went wrong";
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        }

        ErrorResponseDto errorResponse = ErrorResponseDto
                .builder()
                .message(message)
                .build();

        return ResponseEntity.status(httpStatus).body(errorResponse);
    }
}
