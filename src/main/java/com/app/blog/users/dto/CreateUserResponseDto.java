package com.app.blog.users.dto;

import lombok.Data;

@Data
public class CreateUserResponseDto {
    private Long id;

    private String username;

    private String email;


    private String bio;

    private String image;
}
