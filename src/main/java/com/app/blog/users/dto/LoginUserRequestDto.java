package com.app.blog.users.dto;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NonNull;
import lombok.Setter;

@Data
@Setter(AccessLevel.NONE)
public class LoginUserRequestDto {
    @NonNull
    private String username;
    @NonNull
    private String password;
}
