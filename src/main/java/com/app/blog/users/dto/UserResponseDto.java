package com.app.blog.users.dto;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
public class UserResponseDto {
    private Long id;
    private String username;
    private String email;
    private String bio;
    private String image;
}
